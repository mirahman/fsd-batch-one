/**
 * Created by mizan on 12/10/17.
 */

var express = require('express');
var app = express();
var db = require("mysql");

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));


var con = db.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "full_stack"
});


app.get('/',function(req,res){
   res.send('Student API');
});

app.get('/student', function (req, res) {

    var response = [];

    var sql = "select * from students";
    con.query(sql, function (err, students, fields) {
        //if (err) throw err;

        if(!err) {
            //console.log(students);
            response.push({'error': false, 'data' : students});
            res.status(200).send(response);

        } else {
            response.push({'error':true, 'message' : 'NO record found'});
            res.status(404).send(err);
        }

    });
});

app.post('/student', function (req, res) {

    var id = req.body.id;
    var studentName = req.body.studentName;
    var rollNumber = req.body.rollNumber;

    var response = [];

    var sql = "INSERT into students (id,studentName, studentRollNumber) values ('"+id+"','"+studentName+"','"+rollNumber+"')";

    console.log(sql);

    con.query(sql, function (err, result, fields) {

        if(result) {
            response.push({'error': false, 'message' : 'Student Inserted Successfully'});
            res.status(200).send(response);

        } else {
            response.push({'error':true, 'message' : 'could not insert the record'});
            res.status(404).send(response);
        }

    });


});

app.put('/student/:id',function(req,res){

    var id = req.params.id;
    var studentName = req.body.studentName;
    var rollNumber = req.body.rollNumber;

    var response = [];

    var sql = "update students set studentName = '"+studentName+"', studentRollNumber = '"+rollNumber+"' where id = '"+id+"'";

    console.log(sql);

    con.query(sql, function (err, result, fields) {

        if(result) {
            response.push({'error': false, 'message' : 'Student updated Successfully'});
            res.status(200).send(response);

        } else {
            response.push({'error':true, 'message' : 'could not update the record'});
            res.status(404).send(response);
        }

    });


});

app.delete('/student/:id',function(req,res){

    var id = req.params.id;

    var response = [];

    var sql = "delete from students where id = '"+id+"'";

    console.log(sql);

    con.query(sql, function (err, result, fields) {

        if(result) {
            response.push({'error': false, 'message' : 'Student removed Successfully'});
            res.status(200).send(response);

        } else {
            response.push({'error':true, 'message' : 'could not find the record'});
            res.status(404).send(response);
        }

    });

});


app.patch('/student/:id',function(req,res){

    var response = [];

    var authKey = req.header('authorization');

    if(authKey !== "HalaMadrid") {
        response.push({'error':true, 'message' : 'Invalid authorization key'});
        res.status(401).send(response);
    } else {

        var id = req.params.id;
        var fieldName = req.body.fieldName;
        var fieldValue = req.body.fieldValue;



        var sql = "update students set " + fieldName + " = '" + fieldValue + "' where id = '" + id + "'";

        console.log(sql);

        con.query(sql, function (err, result, fields) {

            if (result) {
                response.push({'error': false, 'message': 'Student updated partially Successfully'});
                res.status(200).send(response);

            } else {
                response.push({'error': true, 'message': 'could not update the partial record'});
                res.status(404).send(response);
            }

        });

    }


});




var server = app.listen(5000, function () {
    console.log('Node server is running..');
});
